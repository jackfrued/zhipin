import inspect
import sys

from django.contrib import admin

from common import models


class BaseModelAdmin(admin.ModelAdmin):
    list_per_page = 10
    ordering = ('id', )
    exclude = ('delete_time', 'update_time', 'is_deleted', 'memo')


class UserModelAdmin(BaseModelAdmin):
    list_display = ('id', 'username', 'last_visit', 'is_boss', 'is_locked')
    search_fields = ('username', )


class LoginLogModelAdmin(BaseModelAdmin):
    list_display = ('id', 'user', 'login_time', 'device_code', 'ip_address')


class DistrictModelAdmin(BaseModelAdmin):
    list_display = ('id', 'name', 'is_hot', 'parent_info')
    search_fields = ('id', 'name', )


class IndustryCategoryModelAdmin(BaseModelAdmin):
    list_display = ('id', 'name', 'intro')


class PositionCategoryModelAdmin(BaseModelAdmin):
    list_display = ('id', 'name', 'is_hot')


class SkillTagModelAdmin(BaseModelAdmin):
    list_display = ('id', 'name', 'position_info')


class CompanyModelAdmin(BaseModelAdmin):
    list_display = (
        'id', 'short_name', 'full_name', 'industry_info', 'financing_stage', 'staff_size',
        'web_site', 'intro', 'start_work_time', 'end_work_time', 'overtime_work', 'day_off',
        'editor_info'
    )


class CompanyDetailModelAdmin(BaseModelAdmin):
    list_display = (
        'id', 'company', 'legal_representative', 'registered_capital_info', 'establishment_date',
        'company_type', 'operating_state', 'registered_place', 'uni_credit_code'
    )


class WelfareModelAdmin(BaseModelAdmin):
    list_display = ('id', 'name', 'detail')


class CompanyWelfareModelAdmin(BaseModelAdmin):
    list_display = ('id', 'company', 'welfare')


class CompanyPhotoModelAdmin(BaseModelAdmin):
    list_display = ('id', 'photoname', 'company')


class JobModelAdmin(BaseModelAdmin):
    list_display = (
        'id', 'company_info', 'job_type', 'job_name', 'district_info', 'street_info', 'room_info',
        'work_years', 'min_diploma', 'salary_info', 'user_info', 'pub_date', 'is_open'
    )


class BasicInfoModelAdmin(BaseModelAdmin):
    list_display = (
        'id', 'realname', 'sex', 'birth', 'tel', 'status', 'wechat', 'email', 'intro', 'user'
    )


class DesiredPositionModelAdmin(BaseModelAdmin):
    list_display = (
        'id', 'user_info', 'position_info', 'min_salary', 'max_salary', 'industries', 'district_info'
    )


class JobExperienceModelAdmin(BaseModelAdmin):
    list_display = (
        'id', 'user_info', 'company_name', 'industry_info', 'department_name', 'position_name',
        'position_info', 'start_date', 'end_date', 'skill_tags', 'hide_option', 'company_info',
        'is_current'
    )


class ProjectExperienceModelAdmin(BaseModelAdmin):
    list_display = (
        'id', 'user_info', 'project_name', 'project_role', 'project_url', 'start_date', 'end_date'
    )


class EducationExperienceModelAdmin(BaseModelAdmin):
    list_display = (
        'id', 'user_info', 'school_name', 'education_type', 'diploma', 'major',
        'start_year', 'end_year'
    )


class AttachmentModelAdmin(BaseModelAdmin):
    list_display = ('id', 'user_info', 'name', 'path')


class MessageModelAdmin(BaseModelAdmin):
    list_display = ('id', 'from_user_info', 'to_user_info', 'content', 'insert_time')


# 获取当前模块对应的对象
curr_module = sys.modules[__name__]
# 通过Python反射机制获取models模块中所有的类
cls_members = inspect.getmembers(models, inspect.isclass)
# 循环遍历所有模型类注册对应的模型管理类
for cls_name, cls in cls_members:
    if cls_name != 'BaseModel':
        # 根据模型的名字动态获取模型管理类
        model_admin_cls = getattr(curr_module, f'{cls_name}ModelAdmin')
        if model_admin_cls:
            admin.site.register(cls, model_admin_cls)


# admin.site.register(User, UserModelAdmin)
# admin.site.register(LoginLog, LoginLogModelAdmin)
# admin.site.register(District, DistrictModelAdmin)
# admin.site.register(IndustryCategory, IndustryCategoryModelAdmin)
# admin.site.register(PositionCategory, PositionCategoryModelAdmin)
# admin.site.register(SkillTag, SkillTagModelAdmin)
# admin.site.register(Company, CompanyModelAdmin)
# admin.site.register(CompanyDetail, CompanyDetailModelAdmin)
# admin.site.register(Welfare, WelfareModelAdmin)
# admin.site.register(CompanyWelfare, CompanyWelfareModelAdmin)
# admin.site.register(CompanyPhoto, CompanyPhotoModelAdmin)
# admin.site.register(Job, JobModelAdmin)
# admin.site.register(BasicInfo, BasicInfoModelAdmin)
# admin.site.register(DesiredPosition, DesiredPositionModelAdmin)
# admin.site.register(JobExperience, JobExperienceModelAdmin)
# admin.site.register(ProjectExpereince, ProjectExperienceModelAdmin)
# admin.site.register(EducationExperience, EducationExperienceModelAdmin)
# admin.site.register(Attachment, AttachmentModelAdmin)
# admin.site.register(Message, MessageModelAdmin)
