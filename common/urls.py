"""zhipin URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from rest_framework.routers import DefaultRouter
from rest_framework_swagger.views import get_swagger_view

from common.views import show_provinces, show_district, JobViewSet, CompanyViewSet, login, HotCitiesView, \
    get_mobile_code, upload

doc_view = get_swagger_view(title='招聘网站项目接口文档')

urlpatterns = [
    path('upload/', upload),
    path('districts/', show_provinces),
    path('districts/<int:distid>/', show_district),
    path('hotcities/', HotCitiesView.as_view()),
    path('token/', login),
    path('mobile/<str:tel>/', get_mobile_code),
    path('docs/', doc_view),
]

router = DefaultRouter()
router.register('jobs', JobViewSet)
router.register('companies', CompanyViewSet)
urlpatterns += router.urls
