from django.utils import timezone
from rest_framework import serializers

from common.models import District, Job, BasicInfo, Company, IndustryCategory, Welfare


class DistrictSimpleSerializer(serializers.ModelSerializer):

    class Meta:
        model = District
        fields = ('id', 'name')


class DistrictDetailSerializer(serializers.ModelSerializer):
    cities = serializers.SerializerMethodField()

    @staticmethod
    def get_cities(district):
        queryset = District.objects.filter(parent=district.id).only('name')
        return DistrictSimpleSerializer(queryset, many=True).data

    class Meta:
        model = District
        fields = ('id', 'name', 'is_hot', 'cities')


class IndustryCategorySimpleSerializer(serializers.ModelSerializer):

    class Meta:
        model = IndustryCategory
        fields = ('id', 'name')


class CompanyDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Company
        exclude = ('insert_time', 'delete_time', 'update_time', 'is_deleted', 'memo', 'id')


class CompanySimpleSerializer(serializers.ModelSerializer):
    industry = serializers.SerializerMethodField()
    updated_on = serializers.SerializerMethodField()
    welfares = serializers.SerializerMethodField()

    @staticmethod
    def get_industry(company):
        return IndustryCategorySimpleSerializer(company.industry_category).data

    @staticmethod
    def get_updated_on(company):
        update_time = company.update_time or company.insert_time
        return update_time.strftime('%Y-%m-%d')

    @staticmethod
    def get_welfares(company):
        return [welfare.name for welfare in company.welfares.all()]

    class Meta:
        model = Company
        fields = (
            'id', 'logo', 'short_name', 'staff_size', 'industry',
            'financing_stage', 'web_site', 'intro', 'updated_on', 'welfares'
        )


class JobDetailSerializer(serializers.ModelSerializer):
    district = serializers.SerializerMethodField()
    location = serializers.SerializerMethodField()
    work_years = serializers.SerializerMethodField()
    salary = serializers.SerializerMethodField()
    user = serializers.SerializerMethodField()
    pub_date = serializers.SerializerMethodField()

    @staticmethod
    def get_district(job):
        return DistrictSimpleSerializer(job.district).data

    @staticmethod
    def get_location(job):
        return f'{job.street_info}{job.room_info}'

    @staticmethod
    def get_work_years(job):
        if job.min_work_year == 0:
            return '不限'
        return f'{job.min_work_year}-{job.max_work_year}年'

    @staticmethod
    def get_salary(job):
        return f'{job.min_salary}-{job.max_salary}k {job.salary_month}薪'

    @staticmethod
    def get_user(job):
        info = BasicInfo.objects.filter(user__id=job.user.id).only('realname', 'sex').first()
        if info:
            return f'{info.realname[0:1]}{"先生" if info.sex else "女士"}'
        return ''

    @staticmethod
    def get_pub_date(job):
        delta = timezone.now() - job.pub_date
        if delta.days > 14:
            return job.pub_date.strftime('%Y-%m-%d')
        elif delta.days > 7:
            return '两周以内'
        elif delta.days >= 1:
            return f'{delta.days}天前'
        elif delta.seconds > 3600:
            return f'{delta.seconds // 3600}小时前'
        else:
            return '刚刚发布'

    class Meta:
        model = Job
        fields = (
            'id', 'company', 'job_name', 'job_type', 'district', 'location',
            'work_years', 'min_diploma', 'salary', 'detail', 'user', 'pub_date'
        )
