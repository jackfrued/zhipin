## Django项目中的遗留问题

### 需要补充说明的问题

1. 注册模型管理类时如何去掉重复代码(x)
    - 反射（自省）
    - getattr / setattr
    - inspect.getmembers / ismodule / isclass / isfunction

2. 对接亚马逊S3存储静态资源(x)

3. 多对多关系的处理(x)
    - 有关联查询的地方就有可能出现1+N查询问题，严重影响性能
    - 多对一、一对一外键：
        - queryset.select_related('user', 'district')
    - 多对多外键：
        - query.prefetch_related('...')
        - query.prefetch_related(Prefecth('...', queryset=...))

4. Celery的高级用法(x)
    - 消息代理（broker）：Redis / RabbitMQ（AMQP）
    - 调用异步任务：delay / apply_async
    - 签名：signature / s ---> queue / countdown / expires / serializer
    - 原语：group / chain
    - 路由：celery -A ... worker -Q queue1,queue2
    - 持久化结果：django-celery-results / django-db
    - 定时任务：django-celery-beat

5. 关于日志的补充说明(x)
    - https://zhuanlan.zhihu.com/p/27363484
    
6. 项目上线流程说明(x)
    - 上线前的检查工作
       - settings.py: python manage.py check --depoly
    - 物理架构
        - 创建VPC：公司专属内网环境（服务器部署在内网中）
        - NAT服务：端口映射让外界可以访问到反向代理和跳板机，其他服务器任何端口都不向公网开放
        - 虚拟化部署：用Docker创建虚拟化容器部署项目，通过多容器管理工具（K8S）管理容器
    - 版本控制
        - git clone / git pull
    - uWSGI
        - uwsgi.ini
        - https://uwsgi-docs.readthedocs.io/en/latest/Options.html
    - Nginx
    - HTTPS
        - 购买权威机构颁发的证书：5/6k --- 3/4w
        - 将证书和私钥部署到Nginx ---> 阿里云的HTTPS部署帮助
    - 域名解析
        - 阿里云/腾讯云 ---> 域名解析服务 ---> 把域名解析到云服务器公网IP地址的配置
        - 网站、接口尽量都走HTTPS
    - 进程管理
        - Supervisor（uWSGI、Celery、……）
        - Circus
    - 高并发架构
        - 水平扩展和垂直扩展
        - 裸金属架构 ---> 没有操作系统，直接创建容器使用
        - 网站优化两大定律：
            ~ 使用缓存
            ~ 异步化

### 尚未讲解的问题

1. 如何解决跨域访问数据的问题(x)

    - 同源策略：协议相同、域名相同、端口相同
    - 三方库：django-cors-headers
    
2. 多数据库的配置(x)

    - DATABASE_ROUTERS = [ ... ]
    - db_for_read / db_for_write
    
3. WebSocket的应用(x)
    - pip install channels
    - settings.py 
        INSTALLED_APPS ---> channels
        ASGI_APPLICATION = ...
    - routing.py
        application = ProtocolTypeRouter({
            'websocket': AuthMiddlewareStack(
                URLRouter(websocket_urlpatterns)
            ),
        })
    - chatroom/routing.py
        websocket_urlpatterns = [
            path('ws/chat/', ChatConsumer),
        ]
    - chatroom/views.py
         class ChatConsumer(WebsocketConsumer):
        
            def connect(self):
                self.accept()
        
            def disconnect(self, code):
                pass
        
            def receive(self, text_data=None, bytes_data=None):
                pass
    
    - pip install channels_redis
    - settings.py
        CHANNEL_LAYERS = {
            'default': {
                'BACKEND': 'channels_redis.core.RedisChannelLayer',
                'CONFIG': {
                    "hosts": [
                        ('47.104.31.138', 12345)
                    ],
                },
            },
        }
    -  将websocket加入到组中
        async_to_sync(self.channel_layer.group_add)(
            'chat_channel',
            self.channel_name
        )
        async_to_sync(channel_layer.send)(
            'chat_channel',
            {'message': '...'}
        )
    - 项目部署时，需要部署ASGI服务器（Daphne）
        - daphne -b 本机IP地址 -p 端口 zhipin.asgi:application
        - Nginx配置反向代理将WebSocket请求路由到Daphe
            upstream websocket {
                server 172.31.27.27:8001;
            }
            server {
        
                location /ws/ {
                    proxy_pass http://websocket;
                    proxy_http_version 1.1;
                    proxy_set_header Host $http_host;
                    proxy_set_header X-Real-IP $remote_addr;
                    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                    proxy_set_header X-Forwarded-Host $server_name;
                    proxy_set_header Upgrade $http_upgrade;
                    proxy_set_header Connection "upgrade"; 
                }
            }

4. 单元测试和视图函数的测试(x)

5. 访问控制的两种方式ACL和RBAC
    - 访问控制列表（Access Control List）
    - 基于角色的访问控制（Role-Based Access Control）

6. 项目的虚拟化部署(x)
    - 虚拟化容器：Docker
    - 安装Doker
        - 更新底层库：yum update
        - 移除旧版本：yum erase -y docker docker-io docker-common docker-engine
        - 安装YUM工具：yum install -y yum-utils
        - 添加YUM仓库：yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
        - 安装依赖项：yum install -y device-mapper-persistent-data lvm2
        - 安装Docker社区版：yum install -y docker-ce
        - 启动Docker：systemctl start docker
        
    - Docker相关命令
    
        - 下载镜像：docker pull nginx:latest
        - 查看镜像：docker images
        - 删除镜像：docker rmi <Image ID>
        
        - 创建和运行容器：docker run -d -p 80:80 --name myws nginx:latest
        - 删除容器：docker rm myws2 / docker rm -f myws3
        - 查看运行中的容器：docker ps
        - 停止容器：docker stop myws
        - 启动容器：docker start myws
        - 查看所有容器：docker container ls -a
        
        - 数据卷操作：
            docker run -d -p 80:80 \
            -v /root/code/html:/usr/share/nginx/html \
            --name myws nginx:latest
        
        - 创建MySQL容器（多实例）：    
            docker run -d -p 3306:3306 --name mysql571 \
            -v /root/mysql/data1:/var/lib/mysql \
            -v /root/mysql/log1:/var/log \
            -e "MYSQL_ROOT_PASSWORD=123456" mysql:5.7.30
            
            docker run -d -p 3308:3306 --name mysql572 \
            -v /root/mysql/data2:/var/lib/mysql \
            -v /root/mysql/log2:/var/log \
            -e "MYSQL_ROOT_PASSWORD=123456" mysql:5.7.30
        
        - 创建Redis容器   
            docker run -d -p 54321:6379 --name redis-master \
            redis:5.0.5 redis-server --requirepass 1Qaz2Wsx
            
        - 进入容器：docker exec -it redis-master /bin/bash
        
        - 检查容器：docker inspect <容器名>
        
        - 查看日志：docker logs <容器名>
        
        - 网络操作：
            docker run -d --name redis-slave-1 \
            --link redis-master:redis-master redis:5.0.5 \
            redis-server --slaveof redis-master 6379 --masterauth 1Qaz2Wsx
            
            docker run -d --name redis-slave-2 \
            --link redis-master:redis-master redis:5.0.5 \
            redis-server --slaveof redis-master 6379 --masterauth 1Qaz2Wsx
            
            docker run -d --name redis-slave-3 \
            --link redis-master:redis-master  redis:5.0.5 \
            redis-server --slaveof redis-master 6379 --masterauth 1Qaz2Wsx
        
7. 接口文档的撰写(x)
    - 使用Swagger自动生成接口文档
        - pip install django-rest-swagger
        - urls.py: 
            doc_view = get_swagger_view(title='招聘网站项目接口文档')
            path('docs/', doc_view)
        - settings.py:
            REST_FRAMEWORK = {
                'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema'
            }
    - 在线接口文档撰写平台
        - RAP2
        - 用Docker搭建RAP2私服
            - pip3 install -U docker-compose
            - git clone --depth=1 https://github.com/thx/rap2-delos.git
            - cd rap2-delos
            - docker-compose up -d
            - docker-compose exec delos node scripts/init
            - 防火墙打开3000端口和38080端口
            - http://服务器公网IP地址:3000
            - docker-compose down
