from channels.generic.websocket import WebsocketConsumer

from zhipin import ws_clients


class ChatConsumer(WebsocketConsumer):

    def connect(self):
        ws_clients.append(self)
        self.accept()

    def disconnect(self, code):
        pass

    def receive(self, text_data=None, bytes_data=None):
        pass
