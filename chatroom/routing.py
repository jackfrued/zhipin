from django.urls import path

from chatroom.views import ChatConsumer

websocket_urlpatterns = [
    path('ws/chat/', ChatConsumer),
]
