use zhipin;

-- 插入用户数据
insert into `tb_user`
    (`insert_time`, `delete_time`, `update_time`, `is_deleted`, `memo`, `id`, `username`, `password`, `last_visit`, `is_boss`, `is_locked`) 
values
    ('2020-06-06 15:30:00.000000', null, null, 0, '', 1, '13111112222', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '2020-06-06 15:35:00.000000', 1, 0),
    ('2020-06-06 15:30:00.000000', null, null, 0, '', 2, '13548041193', '059a00192592d5444bc0caad7203f98b506332e2cf7abb35d684ea9bf7c18f08', '2020-06-06 15:35:00.000000', 1, 0),
    ('2020-06-06 15:30:00.000000', null, null, 0, '', 3, '13111112223', 'e4ad93ca07acb8d908a3aa41e920ea4f4ef4f26e7f86cf8291c5db289780a5ae', '2020-06-06 15:35:00.000000', 1, 0),
    ('2020-06-07 15:30:00.000000', null, null, 0, '', 4, '13877778888', '6900ed6019a835783a782ab510084c2440274c6aaa35be61ad83efe884145ff4', '2020-06-08 00:00:00.000000', 0, 0),
    ('2020-06-07 15:30:00.000000', null, null, 0, '', 5, '13877778889', '6900ed6019a835783a782ab510084c2440274c6aaa35be61ad83efe884145ff4', '2020-06-08 00:01:00.000000', 0, 0);

-- 插入用户基本信息
insert into `tb_basic_info`
    (`insert_time`, `delete_time`, `update_time`, `is_deleted`, `memo`, `id`, `realname`, `sex`, `birth`, `tel`, `status`, `wechat`, `email`, `intro`, `user_id`) 
VALUES 
    ('2020-05-30 00:00:00.000000', null, null, 0, '', 1, '张翠山', 1, '1985-12-12', '13900000001', 0, '13900000001', 'zhangcuishan@huawei.com', '', 1),
    ('2020-05-30 00:00:00.000000', null, null, 0, '', 2, '殷素素', 0, '1988-08-08', '13900000002', 0, '13900000002', 'yinsusu@huawei.com', '', 2),
    ('2020-05-30 00:00:00.000000', null, null, 0, '', 5, '殷天正', 1, '1968-05-02', '13900000003', 0, '13900000003', 'yintianzheng@huawei.com', '', 3);
