import json
import random

from datetime import datetime

import MySQLdb


conn = MySQLdb.connect(host='47.104.31.138', port=3306,
                       user='luohao', password='Luohao.618',
                       database='zhipin', charset='utf8')
params = []
with open('company.json', 'r', encoding='utf-8') as file:
    companies_list = json.load(file)
    for company_dict in companies_list:
        params.append((
            datetime.now(), company_dict['logo'], company_dict['short_name'],
            company_dict['full_name'], company_dict['financing_stage'], company_dict['staff_size'],
            company_dict['intro'], company_dict['overtime_work'], company_dict['day_off'],
            random.choice((1, 2, 3)), 1
        ))
try:
    with conn.cursor() as cursor:
        cursor.executemany(
            'insert into tb_company '
            ' (insert_time, logo, short_name, full_name, financing_stage, staff_size, intro, '
            ' overtime_work, day_off, editor_id, industry_category_id) '
            ' values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
            params
        )
        conn.commit()
except MySQLdb.MySQLError as err:
    print(err)
    conn.rollback()
finally:
    conn.close()
