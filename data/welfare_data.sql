use zhipin;

-- 插入福利数据
insert into `tb_welfare`
    (`id`, `logo`, `name`, `detail`)
values
	(1, 'insurance.png', '五险一金', '给予劳动者的保障性待遇包括养老保险、医疗保险、失业保险、工伤保险和生育保险及住房公积金'),
	(2, 'healthcare.png', '定期体检', '在一定的时间内（一般为1年，也可以根据体检者个人的情况具体确定）进行一次全面的体检');

-- 插入公司福利信息数据
INSERT INTO `tb_company_welfare`
    (`insert_time`, `delete_time`, `update_time`, `is_deleted`, `memo`, `id`, `company_id`, `editor_id`, `welfare_id`)
values
	('2020-06-14 19:46:27.186219', null, null, 0, '', 1, 1, 1, 1),
	('2020-06-14 19:48:59.966855', null, null, 0, '', 2, 1, 1, 2);
