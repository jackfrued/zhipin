from django.utils import timezone
from rest_framework.viewsets import ModelViewSet

from common.models import District, IndustryCategory
from common.serializers import DistrictSimpleSerializer
from demo.serializers import DistrictDetailSerialzier, DistrictSerializer, IndustryCategorySerializer


class DistrictViewSet(ModelViewSet):
    queryset = District.objects.all()
    serializer_class = DistrictSimpleSerializer

    def get_serializer_class(self):
        if self.action == 'list':
            return DistrictSimpleSerializer
        elif self.action == 'retrieve':
            return DistrictDetailSerialzier
        return DistrictSerializer


class IndustryCategoryViewSet(ModelViewSet):
    queryset = IndustryCategory.objects.filter(is_deleted=False)
    serializer_class = IndustryCategorySerializer

    def perform_destroy(self, instance):
        instance.is_deleted = True
        instance.delete_time = timezone.now()
        instance.save()
