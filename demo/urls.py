from rest_framework.routers import DefaultRouter

from demo.views import DistrictViewSet, IndustryCategoryViewSet

urlpatterns = []

router = DefaultRouter()
router.register('districts', DistrictViewSet)
router.register('incates', IndustryCategoryViewSet)
urlpatterns += router.urls
