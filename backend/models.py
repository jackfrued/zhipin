from django.db import models


class Dept(models.Model):
    dno = models.IntegerField(primary_key=True)
    dname = models.CharField(max_length=10)
    dloc = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'tb_dept'
        app_label = 'hrs'


class Emp(models.Model):
    eno = models.IntegerField(primary_key=True)
    ename = models.CharField(max_length=20)
    job = models.CharField(max_length=20)
    mgr = models.ForeignKey('self', models.DO_NOTHING, db_column='mgr', blank=True, null=True)
    sal = models.IntegerField()
    comm = models.IntegerField(blank=True, null=True)
    dno = models.ForeignKey(Dept, models.DO_NOTHING, db_column='dno', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tb_emp'
        app_label = 'hrs'
